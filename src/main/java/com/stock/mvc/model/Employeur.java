package com.stock.mvc.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="EMPLOYEUR")
public class Employeur implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "IDEMPLOYEUR")
	private Long idEmployeur;
	@Column(name = "RAISONSOCIALE")
	private String raisonsociale;
	@Column(name = "SIRET")
	private String siret;
	@Column(name = "CODEAPE")
	private int codeAPE;
	
	@OneToMany(mappedBy="idEmployeur")
	private List<Salaire> salaire;
	
	//Getters et Setters
	public Long getIdEmployeur() {
		return idEmployeur;
	}
	public void setIdEmployeur(Long idEmployeur) {
		this.idEmployeur = idEmployeur;
	}
	public String getRaisonsociale() {
		return raisonsociale;
	}
	public void setRaisonsociale(String raisonsociale) {
		this.raisonsociale = raisonsociale;
	}
	public String getSiret() {
		return siret;
	}
	public void setSiret(String siret) {
		this.siret = siret;
	}
	public int getCodeAPE() {
		return codeAPE;
	}
	public void setCodeAPE(int codeAPE) {
		this.codeAPE = codeAPE;
	}
	
	
	public List<Salaire> getSalaire() {
		return salaire;
	}
	public void setSalaire(List<Salaire> salaire) {
		this.salaire = salaire;
	}
	
	//Constructors
	public Employeur(Long idEmployeur, String raisonsociale, String siret, int codeAPE, List<Salaire> salaire) {
		super();
		this.idEmployeur = idEmployeur;
		this.raisonsociale = raisonsociale;
		this.siret = siret;
		this.codeAPE = codeAPE;
		this.salaire = salaire;
	}
	
	public Employeur() {
		super();
	}
	
	
}
