package com.stock.mvc.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="RETRAITE")
public class Retraite implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "IDRETRAITE")
	private Long idRetraite;
	@Column(name = "SECUPLAFON")
	private BigDecimal secuPlafon;
	@Column(name = "SECUDEPLAFON")
	private BigDecimal secuDeplafon;
	@Column(name = "COMPLET1")
	private BigDecimal compleT1;
	@Column(name = "COMPLET2")
	private BigDecimal compleT2;
	@Column(name = "SUPPLE")
	private BigDecimal supple;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "retraite_idSalaire",unique=true)
	private Salaire salaire;
	
	//Getters et Setters
	public Long getIdRetraite() {
		return idRetraite;
	}

	public void setIdRetraite(Long idRetraite) {
		this.idRetraite = idRetraite;
	}

	public BigDecimal getSecuPlafon() {
		return secuPlafon;
	}

	public void setSecuPlafon(BigDecimal secuPlafon) {
		this.secuPlafon = secuPlafon;
	}

	public BigDecimal getSecuDeplafon() {
		return secuDeplafon;
	}

	public void setSecuDeplafon(BigDecimal secuDeplafon) {
		this.secuDeplafon = secuDeplafon;
	}

	public BigDecimal getCompleT1() {
		return compleT1;
	}

	public void setCompleT1(BigDecimal compleT1) {
		this.compleT1 = compleT1;
	}

	public BigDecimal getCompleT2() {
		return compleT2;
	}

	public void setCompleT2(BigDecimal compleT2) {
		this.compleT2 = compleT2;
	}

	public BigDecimal getSupple() {
		return supple;
	}

	public void setSupple(BigDecimal supple) {
		this.supple = supple;
	}

	public Salaire getSalaire() {
		return salaire;
	}

	public void setSalaire(Salaire salaire) {
		this.salaire = salaire;
	}
	
	//Constructors
	public Retraite(Long idRetraite, BigDecimal secuPlafon, BigDecimal secuDeplafon, BigDecimal compleT1,
			BigDecimal compleT2, BigDecimal supple, Salaire salaire) {
		super();
		this.idRetraite = idRetraite;
		this.secuPlafon = secuPlafon;
		this.secuDeplafon = secuDeplafon;
		this.compleT1 = compleT1;
		this.compleT2 = compleT2;
		this.supple = supple;
		this.salaire = salaire;
	}

	
	public Retraite() {
		super();
	}
	
	
}
