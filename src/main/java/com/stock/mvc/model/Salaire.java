package com.stock.mvc.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SALAIRE")
public class Salaire implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "IDSALAIRE")
	private Long idSalaire;
	@Column(name = "SALAIRENET")
	private BigDecimal salaireNet;
	@Column(name = "CONGESPAYES")
	private BigDecimal congesPayes;
	@Column(name = "DATENETPAYE")
	private String dateNetPaye;
	
	@Column(name = "IDEMPLOYEUR",nullable = false)
	private Long idEmployeur;

	//Setters et Getters
	public Long getIdSalaire() {
		return idSalaire;
	}

	public void setIdSalaire(Long idSalaire) {
		this.idSalaire = idSalaire;
	}

	public BigDecimal getSalaireNet() {
		return salaireNet;
	}

	public void setSalaireNet(BigDecimal salaireNet) {
		this.salaireNet = salaireNet;
	}

	public BigDecimal getCongesPayes() {
		return congesPayes;
	}

	public void setCongesPayes(BigDecimal congesPayes) {
		this.congesPayes = congesPayes;
	}

	public String getDateNetPaye() {
		return dateNetPaye;
	}

	public void setDateNetPaye(String dateNetPaye) {
		this.dateNetPaye = dateNetPaye;
	}

	public Long getIdEmployeur() {
		return idEmployeur;
	}

	public void setIdEmployeur(Long idEmployeur) {
		this.idEmployeur = idEmployeur;
	}
	
	//Constructors
	public Salaire(Long idSalaire, BigDecimal salaireNet, BigDecimal congesPayes, String dateNetPaye,
			Long idEmployeur) {
		super();
		this.idSalaire = idSalaire;
		this.salaireNet = salaireNet;
		this.congesPayes = congesPayes;
		this.dateNetPaye = dateNetPaye;
		this.idEmployeur = idEmployeur;
	}

	public Salaire() {
		super();
	}
	
	
}
