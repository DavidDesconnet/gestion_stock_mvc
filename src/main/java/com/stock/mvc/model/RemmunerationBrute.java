package com.stock.mvc.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="REMMUNERATIONBRUTE")
public class RemmunerationBrute implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "IDREMMUNERATIONBRUTE")
	private Long idRemmunerationBrute;
	@Column(name = "SALAIREBASE")
	private BigDecimal salaireBase;
	@Column(name = "ABSCENCENR")
	private BigDecimal abscenceNR;
	@Column(name = "HEURESUPP")
	private BigDecimal heureSupp;
	@Column(name = "INDEMNITENS")
	private BigDecimal indemniteNS;
	@Column(name = "PRIME")
	private BigDecimal prime;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "remmunerationBrute_idSalaire",unique=true)
	private Salaire salaire;

}
