package com.stock.mvc.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="IMPOTS")
public class Impots implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "IDIMPOTS")
	private Long idImpots;
	@Column(name = "NETAVANTIMPOTS")
	private BigDecimal netAvantImpot;
	@Column(name = "TOTALVERSEEMPLOYEUR")
	private BigDecimal totalVerseEmployeur;
	@Column(name = "BASEIMPOT")
	private BigDecimal baseImpot;
	@Column(name = "TAUXPERSONNALISE")
	private BigDecimal tauxPersonnalise;
	@Column(name = "MONTANTPRELEVIMPOT")
	private BigDecimal montantPrelevImpot;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "impots_idSalaire",unique=true)
	private Salaire salaire;
	
	//Getters et Setters
	public Long getIdImpots() {
		return idImpots;
	}

	public void setIdImpots(Long idImpots) {
		this.idImpots = idImpots;
	}

	public BigDecimal getNetAvantImpot() {
		return netAvantImpot;
	}

	public void setNetAvantImpot(BigDecimal netAvantImpot) {
		this.netAvantImpot = netAvantImpot;
	}

	public BigDecimal getTotalVerseEmployeur() {
		return totalVerseEmployeur;
	}

	public void setTotalVerseEmployeur(BigDecimal totalVerseEmployeur) {
		this.totalVerseEmployeur = totalVerseEmployeur;
	}

	public BigDecimal getBaseImpot() {
		return baseImpot;
	}

	public void setBaseImpot(BigDecimal baseImpot) {
		this.baseImpot = baseImpot;
	}

	public BigDecimal getTauxPersonnalise() {
		return tauxPersonnalise;
	}

	public void setTauxPersonnalise(BigDecimal tauxPersonnalise) {
		this.tauxPersonnalise = tauxPersonnalise;
	}

	public BigDecimal getMontantPrelevImpot() {
		return montantPrelevImpot;
	}

	public void setMontantPrelevImpot(BigDecimal montantPrelevImpot) {
		this.montantPrelevImpot = montantPrelevImpot;
	}

	public Salaire getSalaire() {
		return salaire;
	}

	public void setSalaire(Salaire salaire) {
		this.salaire = salaire;
	}
	
	//Constructors
	public Impots(Long idImpots, BigDecimal netAvantImpot, BigDecimal totalVerseEmployeur, BigDecimal baseImpot,
			BigDecimal tauxPersonnalise, BigDecimal montantPrelevImpot, Salaire salaire) {
		super();
		this.idImpots = idImpots;
		this.netAvantImpot = netAvantImpot;
		this.totalVerseEmployeur = totalVerseEmployeur;
		this.baseImpot = baseImpot;
		this.tauxPersonnalise = tauxPersonnalise;
		this.montantPrelevImpot = montantPrelevImpot;
		this.salaire = salaire;
	}

	public Impots() {
		super();
	}
	
}
