package com.stock.mvc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SALARIE")
public class Salarie implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "IDSALARIE")
	private Long idSalarie;
	@Column(name = "NOM")
	private String nom;
	@Column(name = "PRENOM")
	private String prenom;
	@Column(name = "NUMSECU")
	private int numsecu;
	
	@ManyToOne
	@JoinColumn(name="idEmployeur")
	private Employeur employeur;
	
	//Getters et Setters
	public Long getIdSalarie() {
		return idSalarie;
	}
	public void setIdSalarie(Long idSalarie) {
		this.idSalarie = idSalarie;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getNumsecu() {
		return numsecu;
	}
	public void setNumsecu(int numsecu) {
		this.numsecu = numsecu;
	}
	
	//Constructors
	public Salarie(Long idSalarie, String nom, String prenom, int numsecu, Employeur employeur) {
		super();
		this.idSalarie = idSalarie;
		this.nom = nom;
		this.prenom = prenom;
		this.numsecu = numsecu;
		this.employeur = employeur;
	}
	public Salarie() {
		super();
	}
	
	
	
}
