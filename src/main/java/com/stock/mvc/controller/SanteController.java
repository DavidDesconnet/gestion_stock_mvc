package com.stock.mvc.controller;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SanteController {
	@RequestMapping(value = "/sante", method = RequestMethod.GET)
	public String sante(Locale locale, Model model) {
		return "sante/sante";
	}
}
