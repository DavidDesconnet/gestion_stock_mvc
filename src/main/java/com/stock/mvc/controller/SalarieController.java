package com.stock.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.model.Salarie;
import com.stock.mvc.services.ISalarieService;

@Controller
@RequestMapping(value = "/salarie", method = RequestMethod.GET)
public class SalarieController {
	@Autowired
	private ISalarieService salarieservice;
	@RequestMapping(value = "")
	public String salarie(Model model) {
		List<Salarie> salaries = salarieservice.selectAll();
		if(salaries==null) {
			salaries=new ArrayList<Salarie>();
		}
		model.addAttribute("salaries",salaries);
		return "salarie/salarie";
	}
	
	@RequestMapping(value ="/nouveau", method = RequestMethod.GET)
	public String ajouterSalarie(Model model) {
		Salarie salarie = new Salarie();
		model.addAttribute("salarie",salarie);
		return "salarie/ajouterSalarie";
	}
	
	@RequestMapping(value ="/enregistrer", method = RequestMethod.POST)
	public String enregistrerSalarie(Model model, Salarie salarie) {
		if(salarie.getIdSalarie() != null) {
			salarieservice.update(salarie);
		} else {
			salarieservice.save(salarie);
		}
		return "redirect:/salarie";
	}
	
	@RequestMapping(value ="/modifier/{idSalarie}")
	public String modifierSalarie(Model model, @PathVariable Long idSalarie) {
		if(idSalarie != null) {
			Salarie salarie = salarieservice.getById(idSalarie);
			if(salarie != null) {
				model.addAttribute("salarie",salarie);
			}
		}
		return "salarie/ajouterSalarie";
	}
	
	@RequestMapping(value ="/supprimer/{idSalarie}")
	public String supprimerSalarie(Model model, @PathVariable Long idSalarie) {
		if(idSalarie != null) {
			Salarie salarie = salarieservice.getById(idSalarie);
			if(salarie != null) {
				salarieservice.remove(idSalarie);
			}
		}
		return "redirect:/salarie";
	}
}
