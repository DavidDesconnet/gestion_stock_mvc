package com.stock.mvc.controller;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RemmunerationBruteController {
	@RequestMapping(value = "/remmunerationbrute", method = RequestMethod.GET)
	public String remmunerationbrute(Locale locale, Model model) {
		return "remmunerationbrute/remmunerationbrute";
	}
}
