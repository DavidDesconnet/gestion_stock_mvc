package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IRemmunerationBruteDao;
import com.stock.mvc.model.RemmunerationBrute;
import com.stock.mvc.services.IRemmunerationBruteService;

@Transactional
public class RemmunerationBruteServiceImpl implements IRemmunerationBruteService {
private IRemmunerationBruteDao dao;
	
	//Setter
	public void setDao(IRemmunerationBruteDao dao) {
		this.dao = dao;
	}

	@Override
	public RemmunerationBrute save(RemmunerationBrute entity) {
		return dao.save(entity);
	}

	@Override
	public RemmunerationBrute update(RemmunerationBrute entity) {
		return dao.update(entity);
	}

	@Override
	public List<RemmunerationBrute> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<RemmunerationBrute> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField,sort);
	}

	@Override
	public RemmunerationBrute getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public RemmunerationBrute findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public RemmunerationBrute findOne(String paramName, String paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
}
