package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.model.RemmunerationBrute;

public interface IRemmunerationBruteService {
	public RemmunerationBrute save(RemmunerationBrute entity);
	public RemmunerationBrute update(RemmunerationBrute entity);
	public List<RemmunerationBrute> selectAll();
	public List<RemmunerationBrute> selectAll(String sortField, String sort);
	public RemmunerationBrute getById(Long id);
	public void remove(Long id);
	//public RemmunerationBrute findOne(String paramName, Object[] paramValue);
	public RemmunerationBrute findOne(String[] paramNames, Object[] paramValues);
	public RemmunerationBrute findOne(String paramName, String paramValue);
	public int findCountBy(String paramName, String paramValue);
}
