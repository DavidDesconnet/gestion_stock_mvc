package com.stock.mvc.dao;

import com.stock.mvc.model.Salarie;

public interface ISalarieDao extends IGenericDao<Salarie> {

}
