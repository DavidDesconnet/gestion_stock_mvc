package com.stock.mvc.dao;

import com.stock.mvc.model.Impots;

public interface IImpotsDao extends IGenericDao<Impots> {

}
