package com.stock.mvc.dao;

import com.stock.mvc.model.Salaire;

public interface ISalaireDao extends IGenericDao<Salaire> {

}
