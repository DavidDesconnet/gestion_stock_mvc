package com.stock.mvc.dao;

import com.stock.mvc.model.Employeur;

public interface IEmployeurDao extends IGenericDao<Employeur> {

}
