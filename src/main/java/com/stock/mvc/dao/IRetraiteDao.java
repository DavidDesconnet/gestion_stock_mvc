package com.stock.mvc.dao;

import com.stock.mvc.model.Retraite;

public interface IRetraiteDao extends IGenericDao<Retraite> {

}
