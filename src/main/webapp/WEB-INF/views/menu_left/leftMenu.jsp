<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!-- Sidebar -->
<ul
	class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
	id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a
		class="sidebar-brand d-flex align-items-center justify-content-center"
		href="<%=request.getContextPath()%>/resources/index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-laugh-wink"></i>
		</div>
		<div class="sidebar-brand-text mx-3">
			Calev Devise Admin <sup>2</sup>
		</div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- Nav Item - Dashboard -->
	<li class="nav-item"><a class="nav-link" href="index.html"> <i
			class="fa fa-dashboard fa-fw"></i> <fmt:message
				key="common.dashboard" /></a></li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Heading -->
	<div class="sidebar-heading">Interface</div>

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item"><a class="nav-link collapsed" href="#"
		data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
		aria-controls="collapseTwo"> <i class="fas fa-fw fa-cog"></i> <span>Components</span>
	</a>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
			data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">Custom Components:</h6>
				<a class="collapse-item" href="buttons.html">Buttons</a> <a
					class="collapse-item" href="cards.html">Cards</a>
			</div>
		</div></li>

	<!-- Nav Item - Utilities Collapse Menu -->
	<li class="nav-item"><a class="nav-link collapsed" href="#"
		data-toggle="collapse" data-target="#collapseUtilities"
		aria-expanded="true" aria-controls="collapseUtilities"> <i
			class="fas fa-fw fa-wrench"></i> <span>Utilities</span>
	</a>
		<div id="collapseUtilities" class="collapse"
			aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">Custom Utilities:</h6>
				<a class="collapse-item"
					href="<%=request.getContextPath()%>/resources/utilities-color.html">Colors</a>
				<a class="collapse-item"
					href="<%=request.getContextPath()%>/resources/utilities-border.html">Borders</a>
				<a class="collapse-item"
					href="<%=request.getContextPath()%>/resources/utilities-animation.html">Animations</a>
				<a class="collapse-item"
					href="<%=request.getContextPath()%>/resources/utilities-other.html">Other</a>
			</div>
		</div></li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Heading -->
	<!-- Nav Item - Dashboard -->
	<!--       		<li class="nav-item"> -->
	<%--         		<a class="nav-link" href="${pageContext.servletContext.contextPath}/salarie"> --%>
	<!--           			<i class="fa fa-dashboard fa-fw"></i> -->
	<%--           			<fmt:message key="common.salarie"/> --%>
	<!--           		</a> -->
	<!--       		</li> -->
	<c:url value="/salarie" var="salarie"></c:url>
	<li class="nav-item"><a class="nav-link" href="${salarie }"> <i
			class="fa fa-dashboard fa-fw"></i> <fmt:message key="common.salarie" />
	</a></li>
	<!--       		<li class="nav-item"> -->
	<%--         		<a class="nav-link" href="${pageContext.servletContext.contextPath}/employeur"> --%>
	<!--           			<i class="fa fa-dashboard fa-fw"></i> -->
	<%--           			<fmt:message key="common.employeur"/> --%>
	<!--           		</a> -->
	<!--       		</li> -->
	<c:url value="/employeur" var="employeur"></c:url>
	<li class="nav-item"><a class="nav-link" href="${employeur }">
			<i class="fa fa-dashboard fa-fw"></i> <fmt:message
				key="common.employeur" />
	</a></li>
	<!--       		<li class="nav-item"> -->
	<%--         		<a class="nav-link" href="${pageContext.servletContext.contextPath}/remmunerationbrute"> --%>
	<!--           			<i class="fa fa-dashboard fa-fw"></i> -->
	<%--           			<fmt:message key="common.remunerationbrute"/> --%>
	<!--           		</a> -->
	<!--       		</li> -->
	<c:url value="/remmunerationbrute" var="remmunerationbrute"></c:url>
	<li class="nav-item"><a class="nav-link"
		href="${remmunerationbrute }"> <i class="fa fa-dashboard fa-fw"></i>
			<fmt:message key="common.remunerationbrute" />
	</a></li>
	<!--       		<li class="nav-item"> -->
	<%--         		<a class="nav-link" href="${pageContext.servletContext.contextPath}/impots"> --%>
	<!--           			<i class="fa fa-dashboard fa-fw"></i> -->
	<%--           			<fmt:message key="common.impots"/> --%>
	<!--           		</a> -->
	<!--       		</li> -->
	<c:url value="/impots" var="impots"></c:url>
	<li class="nav-item"><a class="nav-link" href="${impots }"> <i
			class="fa fa-dashboard fa-fw"></i> <fmt:message key="common.impots" />
	</a></li>
	<!--       		<li class="nav-item"> -->
	<%--         		<a class="nav-link" href="${pageContext.servletContext.contextPath}/salaire"> --%>
	<!--           			<i class="fa fa-dashboard fa-fw"></i> -->
	<%--           			<fmt:message key="common.salaire"/> --%>
	<!--           		</a> -->
	<!--       		</li> -->
	<c:url value="/salaire" var="salaire"></c:url>
	<li class="nav-item"><a class="nav-link" href="${salaire }"> <i
			class="fa fa-dashboard fa-fw"></i> <fmt:message key="common.salaire" />
	</a></li>
	<!--       		<li class="nav-item"> -->
	<%--         		<a class="nav-link" href="${pageContext.servletContext.contextPath}/sante"> --%>
	<!--           			<i class="fa fa-dashboard fa-fw"></i> -->
	<%--           			<fmt:message key="common.sante"/> --%>
	<!--           		</a> -->
	<!--       		</li> -->
	<c:url value="/sante" var="sante"></c:url>
	<li class="nav-item"><a class="nav-link" href="${sante }"> <i
			class="fa fa-dashboard fa-fw"></i> <fmt:message key="common.sante" />
	</a></li>
	<!--       		<li class="nav-item"> -->
	<%--         		<a class="nav-link" href="${pageContext.servletContext.contextPath}/retraite"> --%>
	<!--           			<i class="fa fa-dashboard fa-fw"></i> -->
	<%--           			<fmt:message key="common.retraite"/> --%>
	<!--           		</a> -->
	<!--       		</li> -->
	<c:url value="/retraite" var="retraite"></c:url>
	<li class="nav-item"><a class="nav-link" href="${retraite }">
			<i class="fa fa-dashboard fa-fw"></i> <fmt:message
				key="common.retraite" />
	</a></li>

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item active"><a class="nav-link" href="#"
		data-toggle="collapse" data-target="#collapsePages"
		aria-expanded="true" aria-controls="collapsePages"> <i
			class="fas fa-fw fa-folder"></i> <span>Pages</span>
	</a>
		<div id="collapsePages" class="collapse show"
			aria-labelledby="headingPages" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">Login Screens:</h6>
				<a class="collapse-item"
					href="<%=request.getContextPath()%>/resources/login.html">Login</a>
				<a class="collapse-item"
					href="<%=request.getContextPath()%>/resources/register.html">Register</a>
				<a class="collapse-item"
					href="<%=request.getContextPath()%>/resources/forgot-password.html">Forgot
					Password</a>
				<div class="collapse-divider"></div>
				<h6 class="collapse-header">Other Pages:</h6>
				<!-- Autre solution les ".." permette de revenir en arri�re <a class="collapse-item" href="../salarie">Salari�</a> -->
				<%-- <a class="collapse-item" href="${pageContext.servletContext.contextPath}/salarie">Salari�</a> --%>
				<a class="collapse-item"
					href="<%=request.getContextPath()%>/resources/404.html">404
					Page</a> <a class="collapse-item active"
					href="<%=request.getContextPath()%>/resources/blank.html">Blank
					Page</a>
			</div>
		</div></li>

	<!-- Nav Item - Charts -->
	<li class="nav-item"><a class="nav-link"
		href="<%=request.getContextPath()%>/resources/charts.html"> <i
			class="fas fa-fw fa-chart-area"></i> <span>Charts</span></a></li>

	<!-- Nav Item - Tables -->
	<li class="nav-item"><a class="nav-link"
		href="<%=request.getContextPath()%>/resources/tables.html"> <i
			class="fas fa-fw fa-table"></i> <span>Tables</span></a></li>

	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">

	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>
<!-- End of Sidebar -->